package com.yulan.threadPool.controller;

import com.yulan.threadPool.entity.Mo.CourseOrder;
import com.yulan.threadPool.response.ServiceResponse;
import com.yulan.threadPool.service.CourseOrderService;
import com.yulan.threadPool.service.PressureTestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 压力测试
 */
@RestController
@Slf4j
@RequestMapping("/PressureTest")
public class PressureTestController {

    /**
     * 模拟redis原子自减
     */
    private AtomicInteger store = new AtomicInteger(2000);

    private AtomicInteger index = new AtomicInteger(0);

    @Resource
    PressureTestService pressureTestService;

    @Resource
    CourseOrderService courseOrderService;


    /**
     * 将测试数据打印出来
     *
     * @return
     */
    @PostMapping("/excute")
    public ServiceResponse testExcute(){

        index.getAndIncrement();
        CourseOrder courseOrder = CourseOrder.builder()
                .cid(index.get())
                .uid(index.get())
                .build();
        try{
            //异步操作
            pressureTestService.processMask(new Thread(() -> {
                courseOrderService.save(courseOrder);
                log.info("插入 {} 成功！线程：{}，为您服务", courseOrder.toString(),Thread.currentThread().getName());
            }));
            return  ServiceResponse.createSuccess();
        }catch (Exception e){
            e.printStackTrace();
            return ServiceResponse.createError();
        }
    }

    @PostMapping("reset")
    public ServiceResponse reset(){
        try{
            this.index.set(0);
            log.info(index.get()+"");
            return ServiceResponse.createSuccess();
        }catch (Exception e){
            return ServiceResponse.createError();
        }
    }
}





























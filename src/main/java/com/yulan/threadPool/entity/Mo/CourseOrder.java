package com.yulan.threadPool.entity.Mo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@TableName("new_student_course")
@Builder
public class CourseOrder {
    @TableId(value = "uid", type = IdType.NONE)
    private Integer uid;

    @TableId(value = "cid", type = IdType.NONE)
    private Integer cid;
}

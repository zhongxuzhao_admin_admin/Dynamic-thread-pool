package com.yulan.threadPool.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yulan.threadPool.Mapper.CourseOrderMapper;
import com.yulan.threadPool.entity.Mo.CourseOrder;
import com.yulan.threadPool.service.CourseOrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CourseOrderServiceImpl extends ServiceImpl<CourseOrderMapper, CourseOrder> implements CourseOrderService {


    @Resource
    CourseOrderMapper courseOrderMapper;

    @Override
    public List<CourseOrder> getTeacherAndCourseAndStudent(int tid, int cid) {
        return courseOrderMapper.getTeacherAndCourseAndStudent(tid,cid);
    }
}

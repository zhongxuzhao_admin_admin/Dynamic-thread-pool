package com.yulan.threadPool.thread;

import com.yulan.threadPool.response.ServiceResponse;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 动态线程池
 */
@Slf4j
public class DynamicThreadPool {

    /**
     * 默认核心线程数：5
     */
    private static final int CORE_POOL_SIZE = 5;
    /**
     * 默认最大线程数：10
     */
    private static final int MAX_POOL_SIZE = 10;
    /**
     * 默认队列容量：100
     */
    private static final int QUEUE_CAPACITY = 100;
    /**
     * 默认额外空闲线程存活时间
     */
    private static final Long KEEP_ALIVE_TIME = 10L;



    /**
     * 线程池
     */
    private ThreadPoolExecutor threadPool ;

    public ThreadPoolExecutor getThreadPool(){
        return this.threadPool;
    }

    public DynamicThreadPool(){
        this.threadPool = new ThreadPoolExecutor(
                CORE_POOL_SIZE,
                MAX_POOL_SIZE,
                KEEP_ALIVE_TIME,
                TimeUnit.SECONDS,
                new FixArrayBlockingQueue<>(QUEUE_CAPACITY),
                new ThreadPoolExecutor.CallerRunsPolicy());
    }




    /**
     * 单例线程池
     */
    private static class Singleton{
        private static DynamicThreadPool instance;

        static{
            instance = new DynamicThreadPool();
        }

        public static DynamicThreadPool getInstance(){
            return instance;
        }
    }

    /**
     * 单例
     * @return
     */
    public static DynamicThreadPool getDynamicThreadPool() {
        return Singleton.getInstance();
    }

    /**
     * 初始化的便捷方法
     */
    public static void init(){
        getDynamicThreadPool();
    }

    /**
     * 动态设置核心线程数
     */
    public ServiceResponse setCorePoolSize(int corePoolSize){
        try{
            this.getThreadPool().setCorePoolSize(corePoolSize);
            log.info("设置核心线程数成功,设置之后的核心线程数： {}",this.getThreadPool().getCorePoolSize());
            return  ServiceResponse.createSuccess();
        }catch (Exception e){
            //设置失败
            e.printStackTrace();
            return ServiceResponse.createByErrorMsgData("设置失败",null);
        }
    }

    /**
     *
     * 设置最大线程数
     */
    public ServiceResponse setMaximumPoolSize(int maximumPoolSize){
        try{
            this.getThreadPool().setMaximumPoolSize(maximumPoolSize);
            log.info("设置核心线程数成功,设置之后的最大线程数： {}",this.getThreadPool().getMaximumPoolSize());
            return  ServiceResponse.createSuccess();
        }catch (Exception e){
            //设置失败
            e.printStackTrace();
            return ServiceResponse.createByErrorMsgData("设置失败",null);
        }
    }

    /**
     *
     * 设置最大线程数
     */
    public ServiceResponse setKeepAliveTime(long keepAliveTime,TimeUnit timeUnit){
        try{
            this.getThreadPool().setKeepAliveTime(keepAliveTime,timeUnit);
            log.info("设置线程活跃时间成功,设置之后的活跃时间： {}",this.getThreadPool().getKeepAliveTime(timeUnit));
            return  ServiceResponse.createSuccess();
        }catch (Exception e){
            //设置失败
            e.printStackTrace();
            return ServiceResponse.createByErrorMsgData("设置失败",null);
        }
    }

    /**
     * 设置容量
     */
    public ServiceResponse setCapacity(int capacity){
        try{
            FixArrayBlockingQueue fixArrayBlockingQueue = (FixArrayBlockingQueue)this.getThreadPool().getQueue();
            fixArrayBlockingQueue.setCapacity(capacity);
            log.info("设置之后的队列长度： {}",getQueueCapacity());
            return  ServiceResponse.createSuccess();
        }catch (Exception e){
            e.printStackTrace();;
            return ServiceResponse.createByErrorMsgData("设置失败",null);
        }
    }

    /**
     * 获取队列容量
     */
    public int getQueueCapacity(){
        try{
            FixArrayBlockingQueue  fixArrayBlockingQueue = (FixArrayBlockingQueue)this.getThreadPool().getQueue();
            return fixArrayBlockingQueue.getCapacity();
        }catch (Exception e){
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * 设置线程池的回绝策略
     */
    public ServiceResponse setRejectHander(RejectedExecutionHandler rejectedExecutionHandler){
        try{
            this.getThreadPool().setRejectedExecutionHandler(rejectedExecutionHandler);
            return ServiceResponse.createSuccess();
        }catch (Exception e){
            e.printStackTrace();;
            return ServiceResponse.createError();
        }
    }

    /**
     * 获取回绝策略
     * @return
     */
    public RejectedExecutionHandler getRejectHander(){
        return this.getThreadPool().getRejectedExecutionHandler();
    }

}




















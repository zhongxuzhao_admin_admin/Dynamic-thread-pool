package com.yulan.threadPool.thread;


import java.util.Date;

/**
 * 工作线程
 */
public class WorkThread implements Runnable {

    private String command;


    public WorkThread(String s){
        this.command = s;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " Start. Time = " + new Date());
        processCommand();
        System.out.println(Thread.currentThread().getName() + " End. Time = " + new Date());
    }

    /**
     * 任务线程要执行的命令
     */
    public void processCommand(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString(){
        return this.command;
    }
}
